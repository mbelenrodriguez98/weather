from flask import Flask, render_template, request
# import json to load json data to python dictionary
import json
# urllib.request to make a request to api
import urllib.request
import string


app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html");

@app.route('/weather',methods=['POST','GET'])
def weather():
    if request.method == 'POST':
        city = request.form['city']
    else:
        #for default name mathura
        city = 'mathura'

    # source contain json data from api
    source = urllib.request.urlopen('http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=98dfe7cacd20d133e90dffa806e0d413').read()

    # converting json data to dictionary

    list_of_data = json.loads(source)
    
    temp_k=(list_of_data['main']['temp'])
    temp_c=temp_k - 273.15
    hum=(list_of_data['main']['humidity'])
    
    if temp_c > 15 and hum < 50:
        state='sunny'
    elif temp_c < 10:
        state='snow'
    elif hum >= 50:
        state='rain'
    else:    
        state='normal'

    # data for variable list_of_data
    data = {
        "city": city[0].upper()+city[1:],
        "country_code": str(list_of_data['sys']['country']),
        "coordinate": str(list_of_data['coord']['lon']) + ' ' + str(list_of_data['coord']['lat']),
        "temp": str("{0:.2f}".format(temp_c)) + '°C',
        "pressure": str(list_of_data['main']['pressure']),
        "humidity": str(hum) + '%',
        "state": state,
    }
    print(data) #solo para debug?
    return render_template('weather.html',data=data)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=True)
